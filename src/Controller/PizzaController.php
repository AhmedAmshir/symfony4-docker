<?php

namespace App\Controller;

// use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Service\MessageService;

class PizzaController extends Controller {
    
    public function index() {
        return $this->render('pizza/index.html.twig', [
            'controller_name' => 'PizzaController',
        ]);
    }

    public function messages() {

        $generator = $this->container->get('message_service');
        $data = $generator->getMessage();
        
        return $this->render('pizza/show.html.twig', ['data' => $data]);
        // return new Response ($message);

    }
}
