<?php

namespace App\Service;

class MessageService {

    private $test;

    public function __construct($test) {
        $this->test = $test;
    }

    public function getMessage() {

        $messages = [
            'You did it! You updated the system! Amazing!',
            'That was one of the coolest updates I\'ve seen all day!',
            'Great work! Keep going!'
        ];

        $index = array_rand($messages);

        return ['message' => $messages[$index], 'name' => $this->test];
    }
}
